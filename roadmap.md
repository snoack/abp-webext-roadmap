## Roadmap

1. Chrome manifest v3 changes
   - Adapt for changes to keep Adblock Plus compatible
   - Potentially contribute to Chromium to improve declarativeNetRequest
   - *__Status:__ Waiting to see what exactly we will have to work with, and waiting for a Chromium build we can test the changes we already made against.*
   - *__Steward:__ Sebastian*
2. Archive (release and dev) builds from Chrome Web Store
   - *__Status:__ The logic for downloading the packages from the CWS has landed and is actively scheduled to run once an hour, moving the builds to publicly exposed endpoints is done - the last step is to move changelog generation over to GitLab, which is also a preparation for #3*
   - *__Steward:__ Tristan*
3. Continuous deployments through GitLab CI
   - *__Status__: The preparations for (2) also affect this (moving files), got privileged access credentials to test automated publishing to the CWS, an MR is filed, in review and WIP.*
   - *__Steward:__ Tristan*
4. Introduce a build system from the JavaScript ecosystem, replacing our legacy buildtools
   - *__Status:__ We decided to go with Gulp and have a working POC. Next steps are to verify that the POC is on par with our legacy buildtools and turning the POC into a merge request that can be reviewed.*
   - *__Steward:__ Geo*
5. Microsoft Edge
   - Pivot our work towards new (Chromium-based) Microsoft Edge
     - Test 3.6 and following releases on old + new Microsoft Edge
     - Limit efforts regarding the old Microsoft Edge to fixing regression and critical bugs
     - Stop releasing for the old Microsoft Edge (and remove any related legacy code) once the new Microsoft Edge has been rolled out to the larger public
     - Implement packaging for the new Microsoft Edge (once supported by the Microsoft Store)
   - *__Status:__ Ongoing; waiting for new Microsoft Edge to be released.*
   - *__Steward:__ Geo*
6. Test automation
   - Pipeline failures are frequent/intermittent and require constant effort 😓
   - Migration to container-based runners would help with reliability of CI pipelines
     - *__Status:__ We finished introducing Docker on the GitLab runners, the Docker images for Adblock Plus related CI/CD are now officially in review, an [issue](https://gitlab.com/eyeo/adblockplus/adblockpluschrome/issues/105) for updating the CI configuration afterwards has been filed.*
   - Add CI for test pages (i.e. new commits landing in testpages triggering a fresh pipeline for the latest succeeding adblockpluschrome revision).
     - *__Status:__ There is a [POC](https://gitlab.com/triluc/testpages.adblockplus.org/commit/278608edb7f48fdeb0c9613ece5b5e5feb036128) for the test pages CI.*
   - Adding more test pages to cover missing functionality, edge cases, snippets, notifications etc.
     - *__Status:__ New roadmap item, in-progress.*
   - *__Stewards:__ Tristan, Ross*
7. Experiment with filter matching in WebAssembly
   - *__Status:__ In progress; this is blocked by [core#87](https://gitlab.com/eyeo/adblockplus/adblockpluscore/issues/87) because any new APIs must have tests.*
   - *__Stewards:__ Hubert, Manish*
8. Clean up technical debt, refactoring legacy code and streamlining core APIs
   - *__Status:__ Removed utils module, no progress on removing remaining ext APIs.*
   - *__Stewards:__ Sebastian, Manish*


## Backlog

- Test automation
  - Run tests on the new (Chromium-based) Microsoft Edge
  - Add automation to test the update scenario
  - Cover more scenarios through test pages
- Incremental filter list updates
- Improving startup performance and memory usage
